package com.example.regis_cat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.text.HtmlCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.regis_cat.Fragments.CentralMonitoreoFragment;
import com.example.regis_cat.Fragments.ContactFragment;
import com.example.regis_cat.Fragments.MapsFragment;
import com.example.regis_cat.Fragments.PanovuFragment;
import com.example.regis_cat.Fragments.PtzFragment;
import com.example.regis_cat.Fragments.ReconocimientoFacialFragment;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

import org.jetbrains.annotations.NotNull;

import org.osmdroid.config.Configuration;

import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    ActionBarDrawerToggle toggle;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //PRECARGA DE FRAGMENT AL INICIO
        getSupportFragmentManager().beginTransaction().add(R.id.content, new MapsFragment()).commit();
        setTitle("Inicio");


        //INICIALIZA EL MAPA CON LOS DATOS OBTENIDOS
        Context ctx = getApplicationContext();

        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        //inflate and create the map
        //setContentView(R.layout.activity_main);
        //map = (MapView) findViewById(R.id.mapview);

        /////////// UI
        drawerLayout = findViewById(R.id.drawer);
        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);

        ////////// Setup toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close);
        toggle = setUpDrawerToggle();
        drawerLayout.addDrawerListener(toggle);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        selectItemNav(item);
        return true;
    }

    private void selectItemNav(MenuItem item) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        switch (item.getItemId()) {
            //case R.id.nav_home:
            //    break;
            case R.id.nav_map:
                ft.replace(R.id.content, new MapsFragment()).commit(); //crear fragment del MApa
                break;
            case R.id.nav_contact:
                ft.replace(R.id.content, new ContactFragment()).commit();
                break;
            case R.id.nav_ptz:
                ft.replace(R.id.content, new PtzFragment()).commit();
                break;
            case R.id.nav_panovu:
                ft.replace(R.id.content, new PanovuFragment()).commit();
                break;
            case R.id.nav_recf:
                ft.replace(R.id.content, new ReconocimientoFacialFragment()).commit();
                break;
            case R.id.nav_cm:
                ft.replace(R.id.content, new CentralMonitoreoFragment()).commit();
                break;
        }
        setTitle(item.getTitle());
        drawerLayout.closeDrawers();
    }



    public ActionBarDrawerToggle setUpDrawerToggle() {
        return new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.drawer_open,
                R.string.drawer_close);
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
    return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NonNull android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ArrayList<String> permissionsToRequest = new ArrayList<>();
        for (int i = 0; i < grantResults.length; i++) {
            permissionsToRequest.add(permissions[i]);
        }
        if (permissionsToRequest.size() > 0) {
            ActivityCompat.requestPermissions(
                    this,
                    permissionsToRequest.toArray(new String[0]),
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void requestPermissionsIfNecessary(String[] permissions) {
        ArrayList<String> permissionsToRequest = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                permissionsToRequest.add(permission);
            }
        }
        if (permissionsToRequest.size() > 0) {
            ActivityCompat.requestPermissions(
                    this,
                    permissionsToRequest.toArray(new String[0]),
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }
}