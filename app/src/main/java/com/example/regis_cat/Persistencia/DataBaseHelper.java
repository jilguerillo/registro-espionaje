package com.example.regis_cat.Persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import com.example.regis_cat.Modelo.Target;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String LOGCAT = null;

    public static final String DATABASE_NAME = "database.db";
    public static final String TABLE_NAME = "cam_table";
    public static final String COL_1 = "NSP";
    public static final String COL_2 = "TIPOCAMARA";
    public static final String COL_3 = "UB_REFERENCIAL";
    public static final String COL_4 = "LATITUDE";
    public static final String COL_5 = "LONGITUDE";
    public static final String COL_6 = "COMUNA";
    public static final String COL_7 = "REGION";


    public DataBaseHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        sqLiteDatabase.execSQL("CREATE TABLE " + TABLE_NAME + " (NSP TEXT, TIPOCAMARA TEXT, UB_REFERENCIAL TEXT, LATITUDE TEXT, LONGITUDE TEXT, COMUNA TEXT, REGION TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }


    public boolean insert(String NSP, String TIPOCAMARA, String UB_REFERENCIAL,
                          String LATITUDE, String LONGITUDE, String COMUNA, String REGION){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_1, NSP);
        cv.put(COL_2, TIPOCAMARA);
        cv.put(COL_3, UB_REFERENCIAL);
        cv.put(COL_4, LATITUDE);
        cv.put(COL_5, LONGITUDE);
        cv.put(COL_6, COMUNA);
        cv.put(COL_7, REGION);
        long result = sqLiteDatabase.insert(TABLE_NAME, null, cv);
        if (result == -1)
            return false;
        else
            return true;
    }

    public List<Target> getAllTarget()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM cam_table", null);
        List<Target> target  = new ArrayList<>();
        if(cursor.moveToFirst())
        {
            do{
                target.add(new Target(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6)));
            }while (cursor.moveToNext());
        }
        return target;
    }

    //Para validar si los datos ya fueron ingresados
    public Cursor getAllData() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor res = sqLiteDatabase.rawQuery("select * from cam_table", null);
        return res;
    }

    public boolean update(String NSP, String TIPOCAMARA, String UB_REFERENCIAL, String LATITUDE,
                          String LONGITUDE, String COMUNA, String REGION) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_1, NSP);
        cv.put(COL_2, TIPOCAMARA);
        cv.put(COL_3, UB_REFERENCIAL);
        cv.put(COL_4, LATITUDE);
        cv.put(COL_5, LONGITUDE);
        cv.put(COL_6, COMUNA);
        cv.put(COL_7, REGION);
        sqLiteDatabase.update(TABLE_NAME, cv, "NSP = ?", new String[]{NSP});
        return true;
    }

    public Integer delete(String NSP) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        return sqLiteDatabase.delete(TABLE_NAME, "NSP = ?", new String[]{NSP});
    }
}
