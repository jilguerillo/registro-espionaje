package com.example.regis_cat.Utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.example.regis_cat.Persistencia.DataBaseHelper;
import com.example.regis_cat.R;

public class Lecturas {

    private WeakReference<ByteArrayOutputStream> _byteArrayOutputStream;

    public long validRegistros(Context context) {
        DataBaseHelper baseHelper = new DataBaseHelper(context, "database", null, 1);
        SQLiteDatabase db = baseHelper.getReadableDatabase();
        long cantidad = DatabaseUtils.queryNumEntries(db, "cam_table");
        db.close();
        return cantidad;
    }

    public String[] leer(Context context) {
        InputStream inputStream = context.getResources().openRawResource(R.raw.target);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        _byteArrayOutputStream = new WeakReference<ByteArrayOutputStream>(byteArrayOutputStream);

        try {
            int i = inputStream.read();
            while (i != -1) {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toString().split("\n");
    }

    public String[] leer_2(Context context) {
        InputStream inputStream = context.getResources().openRawResource(R.raw.televigilancia_publica);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        _byteArrayOutputStream = new WeakReference<ByteArrayOutputStream>(byteArrayOutputStream);

        try {
            int i = inputStream.read();
            while (i != -1) {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toString().split("\n");
    }
}
