package com.example.regis_cat.Fragments;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.regis_cat.Utils.Lecturas;
import com.example.regis_cat.R;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.CopyrightOverlay;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;
import org.osmdroid.views.overlay.simplefastpoint.LabelledGeoPoint;
import org.osmdroid.views.overlay.simplefastpoint.SimpleFastPointOverlay;
import org.osmdroid.views.overlay.simplefastpoint.SimpleFastPointOverlayOptions;
import org.osmdroid.views.overlay.simplefastpoint.SimplePointTheme;

import java.util.ArrayList;
import java.util.List;

import com.example.regis_cat.Modelo.Target;
import com.example.regis_cat.Persistencia.DataBaseHelper;


public class MapsFragment extends Fragment {

    private SharedPreferences sharedPreferences;
    private DataBaseHelper db;
    private List<Target> target = new ArrayList<>();


    private RotationGestureOverlay mRotationGestureOverlay;
    private MyLocationNewOverlay myLocationOverlay;
    private CompassOverlay mCompassOverlay = null;


    private MapController mapController;
    private MapView map;

    ImageButton myLocationButton;
    CopyrightOverlay copyrightOverlay;

    public MapsFragment() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_maps, container,false);
        final MapView map = rootView.findViewById(R.id.mapview);
        
        //INICIA CARGA DE DATOS AL MAPA SI CORRESPONDE
        Context ctx = rootView.getContext();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getContext());

        init(ctx);

        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setMultiTouchControls(true);
        IMapController mapController = map.getController();

        mapController.setZoom(3.5);
        map.setTilesScaledToDpi(true);
        map.setDestroyMode(false);

        GeoPoint startPoint = new GeoPoint(-33.43440261, -70.67722995);
        mapController.setCenter(startPoint);
        //mapController.setCenter(startPoint);

        copyrightOverlay = new CopyrightOverlay(ctx);
        map.getOverlays().add(copyrightOverlay);

        // myLocationOverlay
        myLocationOverlay = new MyLocationNewOverlay(map);
        myLocationOverlay.enableMyLocation();

        // TODO manage following
        // myLocationOverlay.enableFollowLocation();
        myLocationOverlay.setDrawAccuracyEnabled(true);
        mapController.setCenter(myLocationOverlay.getMyLocation());
        map.getOverlays().add(myLocationOverlay);

        // Button to find user location.
        myLocationButton = rootView.findViewById(R.id.my_location_button);
        myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapController.setCenter(myLocationOverlay.getMyLocation());
                mapController.setZoom(15.50);
            }
        });

        // TODO manage following
        // myLocationOverlay.enableFollowLocation();
        myLocationOverlay.setDrawAccuracyEnabled(true);
        mapController.setCenter(myLocationOverlay.getMyLocation());
        mapController.setZoom(14.00);
        map.getOverlays().add(myLocationOverlay);

        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////

        //Carga los puntos en el mapa
        List<IGeoPoint> ptz = new ArrayList<>();
        List<IGeoPoint> rec_facial = new ArrayList<>();
        List<IGeoPoint> panovu = new ArrayList<>();

        List<IGeoPoint> sinInfo = new ArrayList<>();

        db = new DataBaseHelper(ctx, "database", null, 1);
        target = db.getAllTarget();
        if(target.size() <= 0){
        }

        for (int i = 0; i < target.size(); i++) {
            Double auxLT = Double.parseDouble(target.get(i).getLATITUDE().replaceAll("\"", ""));
            Double auxLG = Double.parseDouble(target.get(i).getLONGITUDE().replaceAll("\"", ""));
            String tipo = new String();
            if (target.get(i).getTIPOCAMARA() != null){
                tipo = target.get(i).getTIPOCAMARA().replaceAll("\"", "");
            }

            if (auxLT != 0 && auxLG != 0) {
                if (tipo.equalsIgnoreCase("PTZ")) {
                    ptz.add(new LabelledGeoPoint(auxLT, auxLG
                            , " Tipo Cámara: " + tipo +
                            " - " +
                            " Lugar : " + target.get(i).getUB_REFERENCIAL().replaceAll("\"", "")));
                }
                if (tipo.equalsIgnoreCase("Rec.Facial")) {
                    rec_facial.add(new LabelledGeoPoint(auxLT, auxLG
                            , " Tipo Cámara: " + tipo +
                            " - " +
                            " Lugar : " + target.get(i).getUB_REFERENCIAL().replaceAll("\"", "")));
                }
                if (tipo.equalsIgnoreCase("Panovu")) {
                    panovu.add(new LabelledGeoPoint(auxLT, auxLG
                            , " Tipo Cámara: " + tipo +
                            " - " +
                            " Lugar : " + target.get(i).getUB_REFERENCIAL().replaceAll("\"", "")));
                }
                if (!tipo.equalsIgnoreCase("Panovu") || !tipo.equalsIgnoreCase("Rec.Facial")
                || !tipo.equalsIgnoreCase("PTZ")) {
                    sinInfo.add(new LabelledGeoPoint(auxLT, auxLG
                            , " Tipo Cámara: SIN INFO" +
                            " - " +
                            " Lugar : " + target.get(i).getUB_REFERENCIAL().replaceAll("\"", "")));

                }
            }
        }
//      //////////////// crea puntos en el mapa
        SimplePointTheme pt_sinInfo = new SimplePointTheme(sinInfo, false);

        Paint style_sinInfo = new Paint();
        style_sinInfo.setStyle(Paint.Style.FILL);
        style_sinInfo.setColor(Color.parseColor("#FF00FF"));

        SimpleFastPointOverlayOptions opt_sinInfo = SimpleFastPointOverlayOptions.getDefaultStyle()
                .setAlgorithm(SimpleFastPointOverlayOptions.RenderingAlgorithm.NO_OPTIMIZATION)
                .setRadius(8).setIsClickable(true).setCellSize(15).setPointStyle(style_sinInfo);

        ////
        SimplePointTheme pt_panovu = new SimplePointTheme(panovu, false);

        Paint style_panovu = new Paint();
        style_panovu.setStyle(Paint.Style.FILL);
        style_panovu.setColor(Color.parseColor("#00FFFF"));

        SimpleFastPointOverlayOptions opt_panovu = SimpleFastPointOverlayOptions.getDefaultStyle()
                .setAlgorithm(SimpleFastPointOverlayOptions.RenderingAlgorithm.NO_OPTIMIZATION)
                .setRadius(8).setIsClickable(true).setCellSize(15).setPointStyle(style_panovu);

        //////
        SimplePointTheme pt_ptz = new SimplePointTheme(ptz, false);

        Paint style_ptz = new Paint();
        style_ptz.setStyle(Paint.Style.FILL);
        style_ptz.setColor(Color.parseColor("#DC143C"));

        SimpleFastPointOverlayOptions opt_ptz = SimpleFastPointOverlayOptions.getDefaultStyle()
                .setAlgorithm(SimpleFastPointOverlayOptions.RenderingAlgorithm.NO_OPTIMIZATION)
                .setRadius(8).setIsClickable(true).setCellSize(15).setPointStyle(style_ptz);



        ////////////////////////
        SimplePointTheme pt_rec_facial = new SimplePointTheme(rec_facial, false);

        Paint style_rec_facial = new Paint();
        style_rec_facial.setStyle(Paint.Style.FILL);
        style_rec_facial.setColor(Color.parseColor("#7FFF00"));

        SimpleFastPointOverlayOptions opt_rec_facial = SimpleFastPointOverlayOptions.getDefaultStyle()
                .setAlgorithm(SimpleFastPointOverlayOptions.RenderingAlgorithm.NO_OPTIMIZATION)
                .setRadius(8).setIsClickable(true).setCellSize(15).setPointStyle(style_rec_facial);


        // create the overlay with the theme
        final SimpleFastPointOverlay sfpo_panovu = new SimpleFastPointOverlay(pt_panovu, opt_panovu);
        final SimpleFastPointOverlay sfpo_ptz = new SimpleFastPointOverlay(pt_ptz, opt_ptz);
        final SimpleFastPointOverlay sfpo_rec_facial = new SimpleFastPointOverlay(pt_rec_facial, opt_rec_facial);
        final SimpleFastPointOverlay sfpo_sinInfo = new SimpleFastPointOverlay(pt_sinInfo, opt_sinInfo);


        // mensaje por touch en los target
        sfpo_sinInfo.setOnClickListener(new SimpleFastPointOverlay.OnClickListener() {
            @Override
            public void onClick(SimpleFastPointOverlay.PointAdapter points, Integer point) {
                Toast.makeText(map.getContext()
                        , "-> " + ((LabelledGeoPoint) points.get(point)).getLabel()
                        , Toast.LENGTH_SHORT).show();
            }
        });
        // add overlay
        map.getOverlays().add(sfpo_sinInfo);




        sfpo_panovu.setOnClickListener(new SimpleFastPointOverlay.OnClickListener() {
            @Override
            public void onClick(SimpleFastPointOverlay.PointAdapter points, Integer point) {
                Toast.makeText(map.getContext()
                        , "-> " + ((LabelledGeoPoint) points.get(point)).getLabel()
                        , Toast.LENGTH_SHORT).show();
            }
        });
        // add overlay
        map.getOverlays().add(sfpo_panovu);
        //invalidateMapView();

        sfpo_ptz.setOnClickListener(new SimpleFastPointOverlay.OnClickListener() {
            @Override
            public void onClick(SimpleFastPointOverlay.PointAdapter points, Integer point) {
                Toast.makeText(map.getContext()
                        , "-> " + ((LabelledGeoPoint) points.get(point)).getLabel()
                        , Toast.LENGTH_SHORT).show();
            }
        });
        // add overlay
        map.getOverlays().add(sfpo_ptz);

        sfpo_rec_facial.setOnClickListener(new SimpleFastPointOverlay.OnClickListener() {
            @Override
            public void onClick(SimpleFastPointOverlay.PointAdapter points, Integer point) {
                Toast.makeText(map.getContext()
                        , "-> " + ((LabelledGeoPoint) points.get(point)).getLabel()
                        , Toast.LENGTH_SHORT).show();
            }
        });
        // add overlay
        map.getOverlays().add(sfpo_rec_facial);

        //support for map rotation
        //mRotationGestureOverlay = new RotationGestureOverlay(map);
        //mRotationGestureOverlay.setEnabled(true);
        //map.getOverlays().add(this.mRotationGestureOverlay);

        //Brujula
        //mCompassOverlay = new CompassOverlay(ctx, new InternalCompassOrientationProvider(ctx),map);
        //mCompassOverlay.enableCompass();
        //map.getOverlays().add(this.mCompassOverlay);

        map.invalidate();

        return rootView;
    }

    public void init(Context ctx) {
        Lecturas lecturas = new Lecturas();
        if (lecturas.validRegistros(ctx) == 0) {
            String[] datos = lecturas.leer(ctx);
            String[] datos_2 = lecturas.leer_2(ctx);

            DataBaseHelper baseHelper = new DataBaseHelper(ctx, "database", null, 1);
            SQLiteDatabase db = baseHelper.getWritableDatabase();
            db.beginTransaction();

            for (int i = 0; i < datos.length; i++) {

                String[] linea = datos[i].split(",");
                ContentValues registro = new ContentValues();

                registro.put("NSP", linea[0]);
                registro.put("TIPOCAMARA", linea[1]);
                registro.put("UB_REFERENCIAL", linea[2]);
                registro.put("LATITUDE", linea[3]);
                registro.put("LONGITUDE", linea[4]);
                registro.put("COMUNA", linea[5]);

                db.insert("cam_table", null, registro);
            }

            for (int i = 0; i < datos_2.length; i++) {

                String[] linea2 = datos_2[i].split(",");
                ContentValues registro = new ContentValues();

                registro.put("LATITUDE", linea2[0]);
                registro.put("LONGITUDE", linea2[1]);
                registro.put("UB_REFERENCIAL", linea2[2]);
                registro.put("REGION", linea2[3]);
                registro.put("COMUNA", linea2[4]);
                //registro.put("TIPOCAMARA", linea[5]);

                db.insert("cam_table", null, registro);
            }

            Toast.makeText(this.getContext(), "Registros insertados " + datos_2.length + datos.length, Toast.LENGTH_LONG).show();
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
        } else {
            Toast.makeText(this.getContext(), "Datos ya registrados ", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        map = new MapView(getContext());
        map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
    }

    @Override
    public void onPause() {
        super.onPause();
        map = new MapView(getContext());
        map.onPause();
    }
}